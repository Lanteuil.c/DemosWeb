
jQuery(document).ready(function($) {
    $("#formoubli").submit(function (e) { // On sélectionne le formulaire par son identifiant
        e.preventDefault(); // On désactive l'appel du script par le formulaire (évite le rafraichissement de la page)
        var form_data = $(this).serialize(), // on récupère chacune des données du formulaire
            target_url = $(this).attr("action"),
            target_method = $(this).attr("method");
        console.log("form data : ");
        console.log(form_data);

        $.ajax({
            type : 'POST',
            url : 'oubli_ajax.php',
            data : form_data,
            dataType : 'html',
            success : function (code_html, statut) {
                $('#welcomeMsg').html("Vous êtes oubliés (et sans recharger la page)");
                $('#formoubli').fadeOut();
            }
        });
        return false;
    });
});
